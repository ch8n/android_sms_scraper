package smsscraping.com.example.ch8n.smsscraper.calls;

/**
 * Created by ch8n on 19/1/18.
 */

public class CallModel {

    String date;
    String duration;
    String number;
    String cachedFormattedNumber;
    String cachedName;
    String cachedNumberLabel;
    String cachedPhotoUri;

    String type;

    String COUNTRY_ISO;
    String DATA_USAGE;
    String FEATURES;
    String GEOCODED_LOCATION;
    String IS_READ;

    String NUMBER_PRESENTATION;
    String POST_DIAL_DIGITS;


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCachedFormattedNumber() {
        return cachedFormattedNumber;
    }

    public void setCachedFormattedNumber(String cachedFormattedNumber) {
        this.cachedFormattedNumber = cachedFormattedNumber;
    }

    public String getCachedName() {
        return cachedName;
    }

    public void setCachedName(String cachedName) {
        this.cachedName = cachedName;
    }

    public String getCachedNumberLabel() {
        return cachedNumberLabel;
    }

    public void setCachedNumberLabel(String cachedNumberLabel) {
        this.cachedNumberLabel = cachedNumberLabel;
    }

    public String getCachedPhotoUri() {
        return cachedPhotoUri;
    }

    public void setCachedPhotoUri(String cachedPhotoUri) {
        this.cachedPhotoUri = cachedPhotoUri;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCOUNTRY_ISO() {
        return COUNTRY_ISO;
    }

    public void setCOUNTRY_ISO(String COUNTRY_ISO) {
        this.COUNTRY_ISO = COUNTRY_ISO;
    }

    public String getDATA_USAGE() {
        return DATA_USAGE;
    }

    public void setDATA_USAGE(String DATA_USAGE) {
        this.DATA_USAGE = DATA_USAGE;
    }

    public String getFEATURES() {
        return FEATURES;
    }

    public void setFEATURES(String FEATURES) {
        this.FEATURES = FEATURES;
    }

    public String getGEOCODED_LOCATION() {
        return GEOCODED_LOCATION;
    }

    public void setGEOCODED_LOCATION(String GEOCODED_LOCATION) {
        this.GEOCODED_LOCATION = GEOCODED_LOCATION;
    }

    public String getIS_READ() {
        return IS_READ;
    }

    public void setIS_READ(String IS_READ) {
        this.IS_READ = IS_READ;
    }

    public String getNUMBER_PRESENTATION() {
        return NUMBER_PRESENTATION;
    }

    public void setNUMBER_PRESENTATION(String NUMBER_PRESENTATION) {
        this.NUMBER_PRESENTATION = NUMBER_PRESENTATION;
    }

    public String getPOST_DIAL_DIGITS() {
        return POST_DIAL_DIGITS;
    }

    public void setPOST_DIAL_DIGITS(String POST_DIAL_DIGITS) {
        this.POST_DIAL_DIGITS = POST_DIAL_DIGITS;
    }



}
