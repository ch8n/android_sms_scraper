package smsscraping.com.example.ch8n.smsscraper.sms;

/**
 * Created by ch8n on 19/1/18.
 */

public class SmsScraperHelper {

    public static String id = "_id";
    public static String thread_id = "thread_id";
    public static String address = "address";
    public static String person = "person";
    public static String date = "date";
    public static String protocol = "protocol";
    public static String read = "read";
    public static String status = "status";
    public static String type = "type";
    public static String reply_path_present = "reply_path_present";
    public static String subject = "subject";
    public static String body = "body";
    public static String service_center = "service_center";
    public static String locked = "locked";
}
