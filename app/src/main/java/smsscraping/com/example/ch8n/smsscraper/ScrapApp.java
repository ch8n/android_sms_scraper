package smsscraping.com.example.ch8n.smsscraper;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by ch8n on 23/1/18.
 */

public class ScrapApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new DebugTree());


    }
}
