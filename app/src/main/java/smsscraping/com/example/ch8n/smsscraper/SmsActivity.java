package smsscraping.com.example.ch8n.smsscraper;

import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smsscraping.com.example.ch8n.smsscraper.app_list.UserAppListActivity;
import smsscraping.com.example.ch8n.smsscraper.calls.CallLogActivity;
import smsscraping.com.example.ch8n.smsscraper.sms.SmsListAdapter;
import smsscraping.com.example.ch8n.smsscraper.sms.SmsModel;
import smsscraping.com.example.ch8n.smsscraper.sms.SmsScraperHelper;

public class SmsActivity extends AppCompatActivity {


    //=============navigation======//
    @BindView(R.id.btn_sms_log)
    Button btn_sms_log;

    @BindView(R.id.btn_call_log)
    Button btn_call_log;

    @BindView(R.id.btn_applist_log)
    Button btn_applist_log;


    @BindView(R.id.btn_wifi_log)
    Button btn_wifi_log;

    @BindView(R.id.btn_location_log)
    Button btn_location_log;

    @BindView(R.id.btn_bluetooth_log)
    Button btn_bluetooth_log;

    //==============================//

    @BindView(R.id.btn_inbox)
    Button btn_inbox;
    @BindView(R.id.btn_sent)
    Button btn_sent;
    @BindView(R.id.btn_draft)
    Button btn_draft;
    @BindView(R.id.btn_save)
    Button btn_save;


    @BindView(R.id.lv_msg)
    ListView lv_msg;

    private ContentResolver cr;
    private String[] reqCols;
    private SmsListAdapter adapter;
    private final int PERMISSIONS_REQUEST = 1111;
    private ArrayList<String> permissions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);


        permissions = new ArrayList<>();
        permissions.add(Manifest.permission.READ_SMS);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.READ_CALL_LOG);

        checkPermission(permissions);

        // List required columns
        reqCols = new String[]{
                SmsScraperHelper.id,
                SmsScraperHelper.thread_id,
                SmsScraperHelper.address,
                SmsScraperHelper.person,
                SmsScraperHelper.date,
                SmsScraperHelper.protocol,
                SmsScraperHelper.read,
                SmsScraperHelper.status,
                SmsScraperHelper.type,
                SmsScraperHelper.reply_path_present,
                SmsScraperHelper.subject,
                SmsScraperHelper.body,
                SmsScraperHelper.service_center,
                SmsScraperHelper.locked
        };

        // Get Content Resolver object, which will deal with Content Provider
        cr = getContentResolver();

    }


    private void checkPermission(ArrayList<String> permissions) {
        // Here, thisActivity is the current activity

        for (final String permission : permissions) {

            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        permission)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.

                    new AlertDialog.Builder(this)
                            .setMessage("Give Permission Jackass!")
                            .setCancelable(true)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ActivityCompat.requestPermissions(SmsActivity.this,
                                            new String[]{permission},
                                            PERMISSIONS_REQUEST);

                                }
                            })
                            .create()
                            .show();


                } else {

                    // No explanation needed, we can request the permission.

                    ActivityCompat.requestPermissions(this,
                            new String[]{permission},
                            PERMISSIONS_REQUEST);

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            }


        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Toast.makeText(this, "permission applied", Toast.LENGTH_SHORT).show();

                } else {
                    checkPermission(this.permissions);
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }


    @OnClick(R.id.btn_inbox)
    public void showInbox() {
        Uri inboxURI = Uri.parse("content://sms/inbox");

        // Fetch Inbox SMS Message from Built-in Content Provider
        Cursor cInbox = cr.query(inboxURI, reqCols, null, null, null);

        Toast.makeText(this, "Inbox count:" + cInbox.getCount(), Toast.LENGTH_SHORT).show();

        ArrayList<SmsModel> inboxSmsList = new ArrayList<>();


        for (cInbox.moveToFirst(); !cInbox.isAfterLast(); cInbox.moveToNext()) {
            // The Cursor is now set to the right position
            SmsModel smsModel = new SmsModel();

            smsModel.setId(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.id)));
            smsModel.setThread_id(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.thread_id)));
            smsModel.setAddress(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.address)));
            smsModel.setPerson(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.person)));
            smsModel.setDate(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.date)));
            smsModel.setProtocol(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.protocol)));
            smsModel.setRead(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.read)));
            smsModel.setStatus(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.status)));
            smsModel.setType(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.type)));
            smsModel.setReply_path_present(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.reply_path_present)));
            smsModel.setSubject(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.subject)));
            smsModel.setBody(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.body)));
            smsModel.setService_center(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.service_center)));
            smsModel.setLocked(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.locked)));

            inboxSmsList.add(smsModel);
        }


        adapter = new SmsListAdapter(this, inboxSmsList, "inbox");

        lv_msg.setAdapter(adapter);


    }


    @OnClick(R.id.btn_sent)
    public void showSent() {

        Uri sentURI = Uri.parse("content://sms/sent");

        Cursor cInbox = cr.query(sentURI, reqCols, null, null, null);

        Toast.makeText(this, "sent count:" + cInbox.getCount(), Toast.LENGTH_SHORT).show();

        ArrayList<SmsModel> sendSmsList = new ArrayList<>();


        for (cInbox.moveToFirst(); !cInbox.isAfterLast(); cInbox.moveToNext()) {
            // The Cursor is now set to the right position
            SmsModel smsModel = new SmsModel();

            smsModel.setId(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.id)));
            smsModel.setThread_id(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.thread_id)));
            smsModel.setAddress(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.address)));
            smsModel.setPerson(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.person)));
            smsModel.setDate(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.date)));
            smsModel.setProtocol(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.protocol)));
            smsModel.setRead(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.read)));
            smsModel.setStatus(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.status)));
            smsModel.setType(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.type)));
            smsModel.setReply_path_present(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.reply_path_present)));
            smsModel.setSubject(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.subject)));
            smsModel.setBody(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.body)));
            smsModel.setService_center(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.service_center)));
            smsModel.setLocked(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.locked)));

            sendSmsList.add(smsModel);
        }

        adapter = new SmsListAdapter(this, sendSmsList, "sent");

        lv_msg.setAdapter(adapter);


    }


    @OnClick(R.id.btn_draft)
    public void showDraft() {

        Uri draftURI = Uri.parse("content://sms/draft");

        Cursor cInbox = cr.query(draftURI, reqCols, null, null, null);

        Toast.makeText(this, "draft count:" + cInbox.getCount(), Toast.LENGTH_SHORT).show();

        ArrayList<SmsModel> draftSmsList = new ArrayList<>();

        for (cInbox.moveToFirst(); !cInbox.isAfterLast(); cInbox.moveToNext()) {
            // The Cursor is now set to the right position
            SmsModel smsModel = new SmsModel();

            smsModel.setId(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.id)));
            smsModel.setThread_id(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.thread_id)));
            smsModel.setAddress(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.address)));
            smsModel.setPerson(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.person)));
            smsModel.setDate(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.date)));
            smsModel.setProtocol(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.protocol)));
            smsModel.setRead(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.read)));
            smsModel.setStatus(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.status)));
            smsModel.setType(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.type)));
            smsModel.setReply_path_present(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.reply_path_present)));
            smsModel.setSubject(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.subject)));
            smsModel.setBody(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.body)));
            smsModel.setService_center(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.service_center)));
            smsModel.setLocked(cInbox.getString(cInbox.getColumnIndex(SmsScraperHelper.locked)));

            draftSmsList.add(smsModel);
        }

        adapter = new SmsListAdapter(this, draftSmsList, "draft");

        lv_msg.setAdapter(adapter);

    }


    @OnClick(R.id.btn_save)
    public void saveJsonObjet() {

        JsonArray smsArrayList = new Gson().toJsonTree(adapter.getCurrentArrayList()).getAsJsonArray();
        String fileName = adapter.getSmsTag() + ".json";
        try {

            String path = Environment.getExternalStorageDirectory() + File.separator + "SmsScraper/Sms/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            FileWriter fileWriter = new FileWriter(path + fileName);

            fileWriter.write(smsArrayList.toString());

            Toast.makeText(getApplicationContext(), fileName + " sms List saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.btn_sms_log)
    public void gotoSmslogScreen() {
        Toast.makeText(this, "~(>>__<<)~", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_applist_log)
    public void gotoApplistScreen() {
        Intent intent = new Intent(this, UserAppListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.btn_call_log)
    public void gotoCalllogScreen() {
        Intent intent = new Intent(this, CallLogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.btn_wifi_log)
    public void gotoWifilogScreen() {

    }

    @OnClick(R.id.btn_location_log)
    public void gotoLocationlogScreen() {

    }

    @OnClick(R.id.btn_bluetooth_log)
    public void gotoBluetoothlogScreen() {

    }

}
