package smsscraping.com.example.ch8n.smsscraper.app_list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import smsscraping.com.example.ch8n.smsscraper.R;

/**
 * Created by ch8n on 19/1/18.
 */

public class AppListAdapter extends BaseAdapter {

    @BindView(R.id.app_list_data)
    TextView app_list_data;

    private Context context;
    ArrayList<AppModel> appModels;

    private String AppTag;

    public AppListAdapter(Context context, List<AppModel> sampleList, String AppTag) {

        this.context = context;
        appModels = (ArrayList<AppModel>) sampleList;
        this.AppTag = AppTag;

    }


    public ArrayList<AppModel> getCurrentArrayList() {
        return appModels;
    }

    public String getAppTag() {
        return AppTag;
    }


    @Override
    public int getCount() {
        return appModels.size();
    }

    @Override
    public Object getItem(int i) {
        return appModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {

        LayoutInflater inf = LayoutInflater.from(context);
        view = inf.inflate(R.layout.item_app_list_layout, null);

        ButterKnife.bind(this, view);

        app_list_data.setText(
                "packageName: " + appModels.get(pos).getPackageName()
                        + "\n\n" + "versionCode: " + appModels.get(pos).getVersionCode()
                        + "\n\n" + "versionName: " + appModels.get(pos).getVersionName()
                        + "\n\n" + "targetSdkVersion: " + appModels.get(pos).getTargetSdkVersion()
                        + "\n\n" + "minSdkVersion: " + appModels.get(pos).getMinSdkVersion()
                        + "\n\n" + "enabled: " + appModels.get(pos).getEnabled()
                        + "\n\n" + "dataDir: " + appModels.get(pos).getDataDir()
                        + "\n\n" + "sourceDir: " + appModels.get(pos).getSourceDir()
                        + "\n\n" + "category: " + appModels.get(pos).getCategory()
                        + "\n\n" + "AppName: " + appModels.get(pos).getName()
                        + "\n\n" + "appType: " + appModels.get(pos).getAppType()
                        + "\n\n" + "installSource: " + appModels.get(pos).getInstallSource()
                        + "\n\n" + "installLocation: " + appModels.get(pos).getInstallLocation()
                        + "\n\n" + "firstInstall: " + appModels.get(pos).getFirstInstall()
                        + "\n\n" + "lastUpdated: " + appModels.get(pos).getLastUpdated()
                        + "\n\n" + "activityInfos: " + new Gson().toJson(appModels.get(pos).getActivityInfos())
                        + "\n\n" + "featureList: " + new Gson().toJson(appModels.get(pos).getFeatureList())
                        + "\n\n" + "providerList: " + new Gson().toJson(appModels.get(pos).getProviderList())
                        + "\n\n" + "metaData: " + new Gson().toJson(appModels.get(pos).getMetaData())
        );

        return view;
    }
}

