package smsscraping.com.example.ch8n.smsscraper.sms;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import smsscraping.com.example.ch8n.smsscraper.R;

/**
 * Created by ch8n on 19/1/18.
 */

public class SmsListAdapter extends BaseAdapter {


    @BindView(R.id.sms_id)
    TextView sms_id;

    @BindView(R.id.thread_id)
    TextView thread_id;

    @BindView(R.id.address)
    TextView address;

    @BindView(R.id.person)
    TextView person;

    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.protocol)
    TextView protocol;

    @BindView(R.id.read)
    TextView read;

    @BindView(R.id.status)
    TextView status;

    @BindView(R.id.type)
    TextView type;

    @BindView(R.id.reply_path_present)
    TextView reply_path_present;

    @BindView(R.id.subject)
    TextView subject;

    @BindView(R.id.body)
    TextView body;

    @BindView(R.id.service_center)
    TextView service_center;

    @BindView(R.id.locked)
    TextView locked;

    private Context context;
    ArrayList<SmsModel> smsModels;

    private String smsTag;

    public SmsListAdapter(Context context, ArrayList<SmsModel> sampleList, String smsTag) {

        this.context = context;
        smsModels = sampleList;
        this.smsTag = smsTag;

    }


    public ArrayList<SmsModel> getCurrentArrayList(){
        return smsModels;
    }

    public String getSmsTag(){
        return smsTag;
    }


    @Override
    public int getCount() {
        return smsModels.size();
    }

    @Override
    public Object getItem(int i) {
        return smsModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {

        LayoutInflater inf = LayoutInflater.from(context);
        view = inf.inflate(R.layout.item_sms_content_layout, null);

        ButterKnife.bind(this, view);


        sms_id.setText("_id: "+ smsModels.get(pos).getId());
        thread_id.setText("thread_id: "+ smsModels.get(pos).getThread_id());
        address.setText("address: "+ smsModels.get(pos).getAddress());
        person.setText("person: "+ smsModels.get(pos).getPerson());
        date.setText("date: "+ smsModels.get(pos).getDate());
        protocol.setText("protocol: "+ smsModels.get(pos).getProtocol());
        read.setText("read: "+ smsModels.get(pos).getRead());
        status.setText("status: "+ smsModels.get(pos).getStatus());
        type.setText("type: "+ smsModels.get(pos).getType());
        reply_path_present.setText("reply_path_present: "+ smsModels.get(pos).getReply_path_present());
        subject.setText("subject: "+ smsModels.get(pos).getSubject());
        body.setText("body: "+ smsModels.get(pos).getBody());
        service_center.setText("service_center: "+ smsModels.get(pos).getService_center());
        locked.setText("locked: "+ smsModels.get(pos).getLocked());


        return view;
    }
}
