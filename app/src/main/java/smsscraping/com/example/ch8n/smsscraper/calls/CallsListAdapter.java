package smsscraping.com.example.ch8n.smsscraper.calls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import smsscraping.com.example.ch8n.smsscraper.R;

/**
 * Created by ch8n on 19/1/18.
 */

public class CallsListAdapter extends BaseAdapter {

    @BindView(R.id.tv_number)
    TextView number;
    @BindView(R.id.type)
    TextView type;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.duration)
    TextView duration;

    @BindView(R.id.formatted_number)
    TextView formatted_number;

    @BindView(R.id.others)
    TextView others;

    @BindView(R.id.picture)
    ImageView picture;

    private Context context;
    ArrayList<CallModel> callModels;

    private String callTag;

    public CallsListAdapter(Context context, ArrayList<CallModel> sampleList, String callTag) {

        this.context = context;
        callModels = sampleList;
        this.callTag = callTag;

    }


    public ArrayList<CallModel> getCurrentArrayList() {
        return callModels;
    }

    public String getCallTag() {
        return callTag;
    }


    @Override
    public int getCount() {
        return callModels.size();
    }

    @Override
    public Object getItem(int i) {
        return callModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {

        LayoutInflater inf = LayoutInflater.from(context);
        view = inf.inflate(R.layout.item_call_content_layout, null);

        ButterKnife.bind(this, view);


        date.setText("date: " + callModels.get(pos).getDate());
        type.setText("type: " + callModels.get(pos).getType());
        number.setText("number: " + callModels.get(pos).getNumber());
        duration.setText("duration: " + callModels.get(pos).getDuration());
        formatted_number.setText("duration: " + callModels.get(pos).getDuration());
        others.setText(
                "CacheFormat: " + callModels.get(pos).getCachedFormattedNumber()
                        + "\n" + "CACHED_MATCHED_NUMBER: " + callModels.get(pos).getCachedName()
                        + "\n" + "cachedNumberLabel: " + callModels.get(pos).getCachedNumberLabel()
                        + "\n" + "cachedPhotoUri: " + callModels.get(pos).getCachedPhotoUri()
                        + "\n" + "date: " + callModels.get(pos).getDate()
                        + "\n" + "duration: " + callModels.get(pos).getDuration()
                        + "\n" + "number: " + callModels.get(pos).getNumber()
                        + "\n" + "type: " + callModels.get(pos).getType()
                        + "\n" + "cache number: " + callModels.get(pos).getCachedName()
                        + "\n" + "cache number lable: " + callModels.get(pos).getCachedNumberLabel()
                        + "\n" + "photo uri: " + callModels.get(pos).getCachedPhotoUri()
                        + "\n" + "country iso: " + callModels.get(pos).getCOUNTRY_ISO()
                        + "\n" + "data usage: " + callModels.get(pos).getDATA_USAGE()
                        + "\n" + "feature: " + callModels.get(pos).getFEATURES()
                        + "\n" + "geolocation: " + callModels.get(pos).getGEOCODED_LOCATION()
                        + "\n" + "isRead: " + callModels.get(pos).getIS_READ()
                        + "\n" + "presentation: " + callModels.get(pos).getNUMBER_PRESENTATION()
                        + "\n" + "post dial digits: " + callModels.get(pos).getPOST_DIAL_DIGITS()
        );

        return view;
    }
}
