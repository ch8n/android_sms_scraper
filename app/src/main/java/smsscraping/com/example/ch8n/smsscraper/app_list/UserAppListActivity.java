package smsscraping.com.example.ch8n.smsscraper.app_list;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smsscraping.com.example.ch8n.smsscraper.R;
import smsscraping.com.example.ch8n.smsscraper.SmsActivity;
import smsscraping.com.example.ch8n.smsscraper.calls.CallLogActivity;

public class UserAppListActivity extends AppCompatActivity {

    //=============navigation======//
    @BindView(R.id.btn_sms_log)
    Button btn_sms_log;

    @BindView(R.id.btn_call_log)
    Button btn_call_log;

    @BindView(R.id.btn_applist_log)
    Button btn_applist_log;

    @BindView(R.id.btn_wifi_log)
    Button btn_wifi_log;

    @BindView(R.id.btn_location_log)
    Button btn_location_log;

    @BindView(R.id.btn_bluetooth_log)
    Button btn_bluetooth_log;

    //==============================//

    @BindView(R.id.btn_installed)
    Button btn_installed;

    @BindView(R.id.btn_save)
    Button btn_save;

    @BindView(R.id.tv_json)
    TextView tv_json;

    @BindView(R.id.lv_app)
    ListView lv_app;


    private AppListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_app_list);

        ButterKnife.bind(this);


    }


    @OnClick(R.id.btn_installed)
    public void getInstalledApp() {

        List<ApplicationInfo> installedApplist = getPackageManager()
                .getInstalledApplications(PackageManager.GET_META_DATA);

        List<AppModel> appModels = new ArrayList<>();

        for (ApplicationInfo info : installedApplist) {

            AppModel appModel = new AppModel();


            appModel.setInstallSource(getPackageManager().getInstallerPackageName(info.packageName));
            appModel.setPackageName(info.packageName);
            appModel.setTargetSdkVersion(String.valueOf(info.targetSdkVersion));
            appModel.setMinSdkVersion(String.valueOf(info.minSdkVersion));
            appModel.setEnabled(String.valueOf(info.enabled));
            appModel.setDataDir(info.dataDir);
            appModel.setSourceDir(info.sourceDir);

            switch (info.category) {

                case ApplicationInfo.CATEGORY_AUDIO:
                    appModel.setCategory("CATEGORY_AUDIO");
                    //Category for apps which primarily work with audio or music, such as music players.
                    break;

                case ApplicationInfo.CATEGORY_GAME:
                    appModel.setCategory("CATEGORY_GAME");
                    //Category for apps which are primarily games.
                    break;

                case ApplicationInfo.CATEGORY_IMAGE:
                    appModel.setCategory("CATEGORY_IMAGE");
                    //Category for apps which primarily work with images or photos, such as camera or gallery apps.
                    break;

                case ApplicationInfo.CATEGORY_MAPS:
                    appModel.setCategory("CATEGORY_MAPS");
                    //Category for apps which are primarily maps apps, such as navigation apps.
                    break;

                case ApplicationInfo.CATEGORY_NEWS:
                    appModel.setCategory("CATEGORY_NEWS");
                    //Category for apps which are primarily news apps, such as newspapers, magazines, or sports apps.
                    break;

                case ApplicationInfo.CATEGORY_PRODUCTIVITY:
                    appModel.setCategory("CATEGORY_PRODUCTIVITY");
                    //Category for apps which are primarily productivity apps, such as cloud storage or workplace apps.
                    break;

                case ApplicationInfo.CATEGORY_SOCIAL:
                    appModel.setCategory("CATEGORY_SOCIAL");
                    // Category for apps which are primarily social apps, such as messaging, communication, email, or social network apps.
                    break;


                case ApplicationInfo.CATEGORY_VIDEO:
                    appModel.setCategory("CATEGORY_VIDEO");
                    //Category for apps which primarily work with video or movies, such as streaming video apps.
                    break;

                default:
                    appModel.setCategory("CATEGORY_UNDEFINED");
                    //Value when category is undefined.


            }


            appModel.setName(info.name);

            HashMap<String, String> appMetaData = new HashMap<>();

            if (info.metaData != null) {
                Set<String> metaKeys = info.metaData.keySet();

                for (String metakey : metaKeys) {
                    appMetaData.put(metakey, new Gson().toJson(info.metaData.get(metakey)));
                }

            }

            appModel.setMetaData(appMetaData);

            appModels.add(appModel);


        }

        //------------------------------------//

        installedApplist = getPackageManager().getInstalledApplications(PackageManager.MATCH_SYSTEM_ONLY);

        for (ApplicationInfo info : installedApplist) {

            for (AppModel app : appModels) {

                app.setAppType(app.getPackageName()
                        .equalsIgnoreCase(info.packageName) ?
                        "SYSTEM_APP" : "THIRD_PARTY");

            }
        }

        //------------------------------------//

        List<PackageInfo> packedApplist = getPackageManager()
                .getInstalledPackages(PackageManager.GET_ACTIVITIES);


        for (PackageInfo pack : packedApplist) {

            for (AppModel app : appModels) {

                if (app.getPackageName()
                        .equalsIgnoreCase(pack.packageName)) {

                    int count = 0;
                    HashMap<String, String> activityInfos = new HashMap<>();
                    ActivityInfo[] activityList = pack.activities;


                    if (activityList != null) {

                        for (ActivityInfo info : activityList) {

                            count++;

                            activityInfos.put("parent_activity_name " + count, info.parentActivityName == null ? "N/A" : info.parentActivityName);
                            activityInfos.put("boot_aware " + count, String.valueOf(info.directBootAware));
                            activityInfos.put("available_for_other_app " + count, String.valueOf(info.exported));


                            switch (info.screenOrientation) {

                                case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE:
                                    activityInfos.put("screen_orientation " + count, String.valueOf("SCREEN_ORIENTATION_LANDSCAPE"));
                                    break;
                                case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT:
                                    activityInfos.put("screen_orientation " + count, String.valueOf("SCREEN_ORIENTATION_PORTRAIT"));
                                    break;
                                default:
                                    activityInfos.put("screen_orientation " + count, String.valueOf("SCREEN_ORIENTATION_SENSOR"));
                            }




                        }


                        app.setActivityInfos(activityInfos);
                    }

                }

            }


        }

        //------------------------------------//

        packedApplist = getPackageManager()
                .getInstalledPackages(PackageManager.GET_CONFIGURATIONS);

        for (PackageInfo pack : packedApplist) {

            for (AppModel app : appModels) {
                if (app.getPackageName()
                        .equalsIgnoreCase(pack.packageName)) {

                    app.setFirstInstall(String.valueOf(pack.firstInstallTime));

                    switch (pack.installLocation) {
                        case 1:
                            app.setInstallLocation("INTERNAL_ONLY");
                            break;
                        case 2:
                            app.setInstallLocation("PREF_EXTERNAL");
                            break;
                        default:
                            app.setInstallLocation("AUTO");
                    }


                    app.setLastUpdated(String.valueOf(pack.lastUpdateTime));


                    HashMap<String, String> featureList = new HashMap<>();

                    FeatureInfo[] feature = pack.reqFeatures;
                    if (feature != null) {
                        int feat = 1;
                        for (FeatureInfo info : feature) {
                            featureList.put("feature_name" + feat, info.name);
                            feat++;
                        }

                    }

                    app.setFeatureList(featureList);
                    app.setVersionCode(String.valueOf(pack.versionCode));
                    app.setVersionName(pack.versionName);

                }

            }

        }


        //------------------------------------//

        packedApplist = getPackageManager()
                .getInstalledPackages(PackageManager.GET_PERMISSIONS);

        HashMap<String, String> permissionList = new HashMap<>();

        for (PackageInfo pack : packedApplist) {

            for (AppModel appModel : appModels) {

                if (appModel.getPackageName().equalsIgnoreCase(pack.packageName)) {

                    if (pack.requestedPermissions != null) {
                        for (int i = 0; i < pack.requestedPermissions.length; i++) {
                            permissionList.put(pack.requestedPermissions[i], String.valueOf(pack.requestedPermissionsFlags[i]));
                        }
                        appModel.setPermissionList(permissionList);
                    }


                }

            }

        }

        //------------------------------------//

        packedApplist = getPackageManager()
                .getInstalledPackages(PackageManager.GET_PROVIDERS);

        for (PackageInfo pack : packedApplist) {


            HashMap<String, String> providerList = new HashMap<>();

            for (AppModel appModel : appModels) {
                if (appModel.getPackageName().equalsIgnoreCase(pack.packageName)) {

                    if (pack.providers != null) {
                        for (ProviderInfo info : pack.providers) {
                            providerList.put(info.authority, "permission: "
                                    + String.valueOf(info.grantUriPermissions));
                        }

                        appModel.setProviderList(providerList);
                    }


                }

            }
        }

        //------------------------------------//


        adapter = new AppListAdapter(this, appModels, "AppList");

        lv_app.setAdapter(adapter);

        Toast.makeText(this, "App count:" + appModels.size(), Toast.LENGTH_SHORT).show();


    }


    @OnClick(R.id.btn_save)
    public void saveJsonObjet() {

        JsonArray smsArrayList = new Gson().toJsonTree(adapter.getCurrentArrayList()).getAsJsonArray();
        String fileName = adapter.getAppTag() + ".json";
        try {

            String path = Environment.getExternalStorageDirectory() + File.separator + "SmsScraper/applist/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            FileWriter fileWriter = new FileWriter(path + fileName);

            fileWriter.write(smsArrayList.toString());

            Toast.makeText(getApplicationContext(), fileName + " sms List saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.btn_sms_log)
    public void gotoSmslogScreen() {
        Intent intent = new Intent(this, SmsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.btn_call_log)
    public void gotoCalllogScreen() {
        Intent intent = new Intent(this, CallLogActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.btn_wifi_log)
    public void gotoWifilogScreen() {

    }

    @OnClick(R.id.btn_location_log)
    public void gotoLocationlogScreen() {

    }

    @OnClick(R.id.btn_applist_log)
    public void gotoApplistScreen() {
        Toast.makeText(this, "~(>>__<<)~", Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.btn_bluetooth_log)
    public void gotoBluetoothlogScreen() {

    }

}
