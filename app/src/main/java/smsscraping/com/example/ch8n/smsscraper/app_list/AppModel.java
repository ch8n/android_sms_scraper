package smsscraping.com.example.ch8n.smsscraper.app_list;

import java.util.HashMap;

/**
 * Created by ch8n on 19/1/18.
 */

public class AppModel {

    String packageName;
    String versionCode;//packageInfo
    String versionName;//packageInfo
    String targetSdkVersion;
    String minSdkVersion;
    String enabled;
    String dataDir;

    String sourceDir;
    String category;


    String name;
    String appType;//system or 3rd part - appinfo diff flag
    String installSource;//installer package

    String installLocation;//packinfo
    String firstInstall;//packinfo
    String lastUpdated;//packinfo

    HashMap<String, String> activityInfos;
    HashMap<String, String> featureList;
    HashMap<String, String> providerList;


    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }


    public HashMap<String, String> getActivityInfos() {
        return activityInfos;
    }

    public void setActivityInfos(HashMap<String, String> activityInfos) {
        this.activityInfos = activityInfos;
    }

    HashMap<String, String> metaData;

    public HashMap<String, String> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(HashMap<String, String> permissionList) {
        this.permissionList = permissionList;
    }

    HashMap<String, String> permissionList;

    public HashMap<String, String> getFeatureList() {
        return featureList;
    }

    public void setFeatureList(HashMap<String, String> featureList) {
        this.featureList = featureList;
    }


    public HashMap<String, String> getProviderList() {
        return providerList;
    }

    public void setProviderList(HashMap<String, String> providerList) {
        this.providerList = providerList;
    }

    public String getInstallSource() {
        return installSource;
    }

    public void setInstallSource(String installSource) {
        this.installSource = installSource;
    }


    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }


    public String getFirstInstall() {
        return firstInstall;
    }

    public void setFirstInstall(String firstInstall) {
        this.firstInstall = firstInstall;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getTargetSdkVersion() {
        return targetSdkVersion;
    }

    public void setTargetSdkVersion(String targetSdkVersion) {
        this.targetSdkVersion = targetSdkVersion;
    }

    public String getMinSdkVersion() {
        return minSdkVersion;
    }

    public void setMinSdkVersion(String minSdkVersion) {
        this.minSdkVersion = minSdkVersion;
    }


    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getDataDir() {
        return dataDir;
    }

    public void setDataDir(String dataDir) {
        this.dataDir = dataDir;
    }

    public String getSourceDir() {
        return sourceDir;
    }

    public void setSourceDir(String sourceDir) {
        this.sourceDir = sourceDir;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstallLocation() {
        return installLocation;
    }

    public void setInstallLocation(String installLocation) {
        this.installLocation = installLocation;
    }


    public HashMap<String, String> getMetaData() {
        return metaData;
    }

    public void setMetaData(HashMap<String, String> metaData) {
        this.metaData = metaData;
    }
}
