package smsscraping.com.example.ch8n.smsscraper.calls;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CallLog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import smsscraping.com.example.ch8n.smsscraper.R;
import smsscraping.com.example.ch8n.smsscraper.SmsActivity;
import smsscraping.com.example.ch8n.smsscraper.app_list.UserAppListActivity;

public class CallLogActivity extends AppCompatActivity {


    //=============navigation======//
    @BindView(R.id.btn_sms_log)
    Button btn_sms_log;

    @BindView(R.id.btn_call_log)
    Button btn_call_log;

    @BindView(R.id.btn_applist_log)
    Button btn_applist_log;

    @BindView(R.id.btn_wifi_log)
    Button btn_wifi_log;

    @BindView(R.id.btn_location_log)
    Button btn_location_log;

    @BindView(R.id.btn_bluetooth_log)
    Button btn_bluetooth_log;

    //==============================//

    @BindView(R.id.btn_incom)
    Button btn_incom;
    @BindView(R.id.btn_miss)
    Button btn_miss;
    @BindView(R.id.btn_out)
    Button btn_out;
    @BindView(R.id.btn_save)
    Button btn_save;


    @BindView(R.id.lv_calls)
    ListView lv_calls;

    private CallsListAdapter adapter;
    private String[] reqCols;
    private ContentResolver cr;
    private Uri callsURI;
    private Cursor cursorCalls;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_log);

        ButterKnife.bind(this);


        // List required columns
        reqCols = new String[]{
                CallLogHelper.DATE,
                CallLogHelper.DURATION,
                CallLogHelper.NUMBER,
                CallLogHelper.CACHED_FORMATTED_NUMBER,
                CallLogHelper.CACHED_NAME,
                CallLogHelper.CACHED_NUMBER_LABEL,
                CallLogHelper.CACHED_PHOTO_URI,
                CallLogHelper.TYPE,
                CallLogHelper.COUNTRY_ISO,
                CallLogHelper.DATA_USAGE,
                CallLogHelper.FEATURES,
                CallLogHelper.GEOCODED_LOCATION,
                CallLogHelper.IS_READ,
                CallLogHelper.NUMBER_PRESENTATION,
                CallLogHelper.POST_DIAL_DIGITS

        };

        cr = getContentResolver();

        callsURI = Uri.parse("content://call_log/calls");

        // Fetch Inbox SMS Message from Built-in Content Provider
        cursorCalls = cr.query(callsURI, reqCols, null, null, null);


    }


    @OnClick(R.id.btn_incom)
    public void getIncomeCallslogs() {


        ArrayList<CallModel> incomCallList = new ArrayList<>();

        for (cursorCalls.moveToFirst(); !cursorCalls.isAfterLast(); cursorCalls.moveToNext()) {
            // The Cursor is now set to the right position
            CallModel callModel = new CallModel();

            if (cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.TYPE)) == (CallLog.Calls.INCOMING_TYPE)) {
                callModel.setType("INCOMING");
                callModel.setDate(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DATE)));
                callModel.setDuration(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DURATION)));
                callModel.setNumber(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.NUMBER)));
                callModel.setCachedFormattedNumber(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_FORMATTED_NUMBER)));
                callModel.setCachedName(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_NAME)));
                callModel.setCachedNumberLabel(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_NUMBER_LABEL)));
                callModel.setCachedPhotoUri(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_PHOTO_URI)));
                callModel.setType(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.TYPE)));
                callModel.setCOUNTRY_ISO(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.COUNTRY_ISO)));
                callModel.setDATA_USAGE(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DATA_USAGE)));

                int feature = cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.FEATURES));

                switch (feature) {
                    case CallLogHelper.FEATURES_VIDEO:
                        callModel.setFEATURES("VIDEO_CALL");
                        break;
                    case CallLogHelper.FEATURES_PULLED_EXTERNALLY:
                        callModel.setFEATURES("CALL_PULLED_EXTERNALLY");
                        break;
                    case CallLogHelper.FEATURES_WIFI:
                        callModel.setFEATURES("WIFI_CALLING");
                        break;
                    case CallLogHelper.FEATURES_HD_CALL:
                        callModel.setFEATURES("HD_CALLING");
                        break;
                    default:
                        callModel.setFEATURES("UNKNOWN");
                }

                callModel.setGEOCODED_LOCATION(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.GEOCODED_LOCATION)));

                int isRead = cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.IS_READ));

                switch (isRead) {
                    case 1:
                        callModel.setIS_READ("READ_BY_USER");
                        break;
                    default:
                        callModel.setIS_READ("NOT_READ_BY_USER");
                }

                int networkProtocal = cursorCalls.getColumnIndex(CallLogHelper.NUMBER_PRESENTATION);

                switch (networkProtocal) {
                    case CallLogHelper.PRESENTATION_ALLOWED:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_ALLOWED");
                        break;
                    case CallLogHelper.PRESENTATION_RESTRICTED:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_RESTRICTED");
                        break;
                    case CallLogHelper.PRESENTATION_UNKNOWN:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_UNKNOWN");
                        break;
                    case CallLogHelper.PRESENTATION_PAYPHONE:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_PAYPHONE");
                        break;

                    default:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_UNKNOWN");
                }

                callModel.setPOST_DIAL_DIGITS(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.POST_DIAL_DIGITS)));

                incomCallList.add(callModel);
            }


        }


        adapter = new CallsListAdapter(this, incomCallList, "income");

        lv_calls.setAdapter(adapter);

        Toast.makeText(this, "Income call count:" + incomCallList.size(), Toast.LENGTH_SHORT).show();


    }

    @OnClick(R.id.btn_miss)
    public void getMissCallslogs() {

        ArrayList<CallModel> incomCallList = new ArrayList<>();

        for (cursorCalls.moveToFirst(); !cursorCalls.isAfterLast(); cursorCalls.moveToNext()) {
            // The Cursor is now set to the right position
            CallModel callModel = new CallModel();

            if (cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.TYPE)) == (CallLog.Calls.MISSED_TYPE)) {
                callModel.setType("Missed");
                callModel.setDate(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DATE)));
                callModel.setDuration(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DURATION)));
                callModel.setNumber(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.NUMBER)));
                callModel.setCachedFormattedNumber(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_FORMATTED_NUMBER)));
                callModel.setCachedName(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_NAME)));
                callModel.setCachedNumberLabel(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_NUMBER_LABEL)));
                callModel.setCachedPhotoUri(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_PHOTO_URI)));
                callModel.setType(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.TYPE)));
                callModel.setCOUNTRY_ISO(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.COUNTRY_ISO)));
                callModel.setDATA_USAGE(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DATA_USAGE)));

                int feature = cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.FEATURES));

                switch (feature) {
                    case CallLogHelper.FEATURES_VIDEO:
                        callModel.setFEATURES("VIDEO_CALL");
                        break;
                    case CallLogHelper.FEATURES_PULLED_EXTERNALLY:
                        callModel.setFEATURES("CALL_PULLED_EXTERNALLY");
                        break;
                    case CallLogHelper.FEATURES_WIFI:
                        callModel.setFEATURES("WIFI_CALLING");
                        break;
                    case CallLogHelper.FEATURES_HD_CALL:
                        callModel.setFEATURES("HD_CALLING");
                        break;
                    default:
                        callModel.setFEATURES("UNKNOWN");
                }

                callModel.setGEOCODED_LOCATION(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.GEOCODED_LOCATION)));

                int isRead = cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.IS_READ));

                switch (isRead) {
                    case 1:
                        callModel.setIS_READ("READ_BY_USER");
                        break;
                    default:
                        callModel.setIS_READ("NOT_READ_BY_USER");
                }

                int networkProtocal = cursorCalls.getColumnIndex(CallLogHelper.NUMBER_PRESENTATION);

                switch (networkProtocal) {
                    case CallLogHelper.PRESENTATION_ALLOWED:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_ALLOWED");
                        break;
                    case CallLogHelper.PRESENTATION_RESTRICTED:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_RESTRICTED");
                        break;
                    case CallLogHelper.PRESENTATION_UNKNOWN:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_UNKNOWN");
                        break;
                    case CallLogHelper.PRESENTATION_PAYPHONE:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_PAYPHONE");
                        break;

                    default:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_UNKNOWN");
                }

                callModel.setPOST_DIAL_DIGITS(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.POST_DIAL_DIGITS)));

                incomCallList.add(callModel);
            }


        }


        adapter = new CallsListAdapter(this, incomCallList, "missed");

        lv_calls.setAdapter(adapter);

        Toast.makeText(this, "Missed call count:" + incomCallList.size(), Toast.LENGTH_SHORT).show();


    }


    @OnClick(R.id.btn_out)
    public void getOutGoingCallslogs() {

        ArrayList<CallModel> incomCallList = new ArrayList<>();

        for (cursorCalls.moveToFirst(); !cursorCalls.isAfterLast(); cursorCalls.moveToNext()) {
            // The Cursor is now set to the right position
            CallModel callModel = new CallModel();

            if (cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.TYPE)) == (CallLog.Calls.OUTGOING_TYPE)) {
                callModel.setType("Out-going");
                callModel.setDate(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DATE)));
                callModel.setDuration(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DURATION)));
                callModel.setNumber(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.NUMBER)));
                callModel.setCachedFormattedNumber(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_FORMATTED_NUMBER)));
                callModel.setCachedName(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_NAME)));
                callModel.setCachedNumberLabel(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_NUMBER_LABEL)));
                callModel.setCachedPhotoUri(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.CACHED_PHOTO_URI)));
                callModel.setType(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.TYPE)));
                callModel.setCOUNTRY_ISO(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.COUNTRY_ISO)));
                callModel.setDATA_USAGE(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.DATA_USAGE)));

                int feature = cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.FEATURES));

                switch (feature) {
                    case CallLogHelper.FEATURES_VIDEO:
                        callModel.setFEATURES("VIDEO_CALL");
                        break;
                    case CallLogHelper.FEATURES_PULLED_EXTERNALLY:
                        callModel.setFEATURES("CALL_PULLED_EXTERNALLY");
                        break;
                    case CallLogHelper.FEATURES_WIFI:
                        callModel.setFEATURES("WIFI_CALLING");
                        break;
                    case CallLogHelper.FEATURES_HD_CALL:
                        callModel.setFEATURES("HD_CALLING");
                        break;
                    default:
                        callModel.setFEATURES("UNKNOWN");
                }

                callModel.setGEOCODED_LOCATION(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.GEOCODED_LOCATION)));

                int isRead = cursorCalls.getInt(cursorCalls.getColumnIndex(CallLogHelper.IS_READ));

                switch (isRead) {
                    case 1:
                        callModel.setIS_READ("READ_BY_USER");
                        break;
                    default:
                        callModel.setIS_READ("NOT_READ_BY_USER");
                }

                int networkProtocal = cursorCalls.getColumnIndex(CallLogHelper.NUMBER_PRESENTATION);

                switch (networkProtocal) {
                    case CallLogHelper.PRESENTATION_ALLOWED:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_ALLOWED");
                        break;
                    case CallLogHelper.PRESENTATION_RESTRICTED:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_RESTRICTED");
                        break;
                    case CallLogHelper.PRESENTATION_UNKNOWN:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_UNKNOWN");
                        break;
                    case CallLogHelper.PRESENTATION_PAYPHONE:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_PAYPHONE");
                        break;

                    default:
                        callModel.setNUMBER_PRESENTATION("PRESENTATION_UNKNOWN");
                }

                callModel.setPOST_DIAL_DIGITS(cursorCalls.getString(cursorCalls.getColumnIndex(CallLogHelper.POST_DIAL_DIGITS)));

                incomCallList.add(callModel);
            }


        }


        adapter = new CallsListAdapter(this, incomCallList, "outgoin");

        lv_calls.setAdapter(adapter);

        Toast.makeText(this, "Outgoing call count:" + incomCallList.size(), Toast.LENGTH_SHORT).show();

    }


    @OnClick(R.id.btn_save)
    public void saveJsonObjet() {

        JsonArray smsArrayList = new Gson().toJsonTree(adapter.getCurrentArrayList()).getAsJsonArray();
        String fileName = adapter.getCallTag() + ".json";
        try {

            String path = Environment.getExternalStorageDirectory() + File.separator + "SmsScraper/calls/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            FileWriter fileWriter = new FileWriter(path + fileName);

            fileWriter.write(smsArrayList.toString());

            Toast.makeText(getApplicationContext(), fileName + " sms List saved", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.btn_sms_log)
    public void gotoSmslogScreen() {
        Intent intent = new Intent(this, SmsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.btn_call_log)
    public void gotoCalllogScreen() {
        Toast.makeText(this, "~(>>__<<)~", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btn_wifi_log)
    public void gotoWifilogScreen() {

    }

    @OnClick(R.id.btn_location_log)
    public void gotoLocationlogScreen() {

    }

    @OnClick(R.id.btn_applist_log)
    public void gotoApplistScreen() {
        Intent intent = new Intent(this, UserAppListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    @OnClick(R.id.btn_bluetooth_log)
    public void gotoBluetoothlogScreen() {

    }


}
