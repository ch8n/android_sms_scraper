package smsscraping.com.example.ch8n.smsscraper.calls;

import android.net.Uri;
import android.provider.CallLog;

/**
 * Created by ch8n on 19/1/18.
 */

public class CallLogHelper {

    public static final String NUMBER = CallLog.Calls.NUMBER;
    public static final String TYPE = CallLog.Calls.TYPE;
    public static final String DATE = CallLog.Calls.DATE;
    public static final String DURATION = CallLog.Calls.DURATION;

    public static final String CACHED_FORMATTED_NUMBER = CallLog.Calls.CACHED_FORMATTED_NUMBER;
    public static final String CACHED_NAME = "name";
    public static final String CACHED_NUMBER_LABEL = "numberlabel";


    //---------------------------------//

    public static final String CACHED_PHOTO_ID = "photo_id";
    public static final String CACHED_PHOTO_URI = "photo_uri";
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/calls";
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/calls";
    public static final String COUNTRY_ISO = "countryiso";
    public static final String DATA_USAGE = "data_usage";
    public static final String DEFAULT_SORT_ORDER = "date DESC";
    public static final String EXTRA_CALL_TYPE_FILTER = "android.provider.extra.CALL_TYPE_FILTER";
    public static final String FEATURES = "features";
    public static final String GEOCODED_LOCATION = "geocoded_location";
    public static final String IS_READ = "is_read";
    public static final String LAST_MODIFIED = "last_modified";
    public static final String LIMIT_PARAM_KEY = "limit";
    public static final String NEW = "new";
    public static final String NUMBER_PRESENTATION = "presentation";
    public static final String OFFSET_PARAM_KEY = "offset";
    public static final String PHONE_ACCOUNT_COMPONENT_NAME = "subscription_component_name";
    public static final String PHONE_ACCOUNT_ID = "subscription_id";
    public static final String POST_DIAL_DIGITS = "post_dial_digits";


    public static final int FEATURES_HD_CALL = 4;
    public static final int FEATURES_PULLED_EXTERNALLY = 2;
    public static final int FEATURES_VIDEO = 1;
    public static final int FEATURES_WIFI = 8;

    public static final int PRESENTATION_ALLOWED = 1;
    public static final int PRESENTATION_PAYPHONE = 4;
    public static final int PRESENTATION_RESTRICTED = 2;
    public static final int PRESENTATION_UNKNOWN = 3;
}
