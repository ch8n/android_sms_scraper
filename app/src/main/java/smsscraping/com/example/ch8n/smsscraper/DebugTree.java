package smsscraping.com.example.ch8n.smsscraper;

import android.util.Log;

import timber.log.Timber;

/**
 * Created by ch8n on 14/1/18.
 * Usage: Timber.plant(new DebugTree());
 */

public class DebugTree extends Timber.DebugTree {


    private final int MAX_LOG_LENGTH = 4000;

    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return super.createStackElementTag(element) + ":"
                + element.getMethodName() + "\t"
                + element.getLineNumber();
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        super.log(priority, tag, message, t);

        // Message is short enough, doesn't need to be broken into chunks
        if (message.length() < MAX_LOG_LENGTH) {
            if (priority == Log.ASSERT) {
                Log.wtf(tag, message);
            } else {
                Log.println(priority, tag, message);
            }
            return;
        }


        // Split by line, then ensure each line can fit into Log's max length
        for (int i = 0, length = message.length(); i < length; i++) {
            int newline = message.indexOf('\n', i);
            newline = newline != -1 ? newline : length;
            do {
                int end = Math.min(newline, i + MAX_LOG_LENGTH);
                String part = message.substring(i, end);
                if (priority == Log.ASSERT) {
                    Log.wtf(tag, part);
                } else {
                    Log.println(priority, tag, part);
                }
                i = end;
            } while (i < newline);
        }

    }
}
